const express = require('express')
const routes = express.Router()



routes.get('/', (req, res) => {
    req.getConnection((err, conn) => {
        
        if (err) return res.send(err)
        
        conn.query('SELECT * FROM movies ORDER BY id_movie', (err, rows) => {

            if (err) return res.send(err)
            res.json(rows)
           
        })
    })
})

routes.post('/', (req, res) => {
    req.getConnection((err, conn) => {

        if (err) return res.send(err)

        conn.query('INSERT INTO movies set ?',[req.body], (err, rows) => {
            if (err) return res.send(err)

            res.send('Movie insertado!')
        })
    })
})

routes.delete('/:id', (req, res) => {
    req.getConnection((err, conn) => {

        if (err) return res.send(err)

        conn.query('DELETE FROM movies WHERE id = ?',[req.body,req.params], (err, rows) => {
            if (err) return res.send(err)

            res.send('Movie eliminado!')
        })
    })
})

routes.put('/:id', (req, res) => {
    req.getConnection((err, conn) => {

        if (err) return res.send(err)

        conn.query('UPDATE movies SET ? WHERE id = ?',[req.body, req.params.id], (err, rows) => {
            if (err) return res.send(err)

            res.send('Movie actualizado!')
        })
    })
})
module.exports = routes