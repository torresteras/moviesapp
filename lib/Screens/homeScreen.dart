// ignore: file_names
import 'package:flutter/material.dart';
import 'package:moviesapp/Api/api.dart';
import 'package:moviesapp/Model/movies_model.dart';
import 'package:moviesapp/Model/search.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final FetchMovieList _movieList = FetchMovieList();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: const Text('Peliculas'),
          actions: [
            IconButton(
              onPressed: () {
                showSearch(context: context, delegate: SearchMovie());
              },
              icon: const Icon(Icons.search_sharp),
            ),
          ],
        ),
        body: Container(
          color: Colors.black,
          padding: const EdgeInsets.all(20),
          child: FutureBuilder<List<MoviesList>>(
            future: _movieList.getmovieList(),
            builder: (context, snapshot) {
              var data = snapshot.data;
              return ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: data?.length,
                  itemBuilder: (context, index) {
                    if (!snapshot.hasData) {
                      return const Center(child: CircularProgressIndicator());
                    }
                    return Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Card(
                        elevation: 5,
                        margin: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 16.0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0)),
                        color: Colors.grey.shade900,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ListTile(
                            /*title: Wrap(
                              children: [
                                const SizedBox(width: 20.0),*/
                            title: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${data?[index].name}',
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                const SizedBox(height: 10),
                                Image.asset(
                                  //'assets/icons/star-plus-logo.png',
                                  '${data?[index].platform}',
                                  width: 35,
                                  height: 30,
                                )
                                /* Text(
                                  '${data?[index].platform}',
                                  style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                */
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  });
            },
          ),
        ),
      ),
    );
  }
}
