import 'package:flutter/material.dart';
import 'package:moviesapp/Screens/homeScreen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        color: Colors.black,
        child: Column(
            //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ColorFiltered(
                child: Image.asset(
                  'assets/images/tv_icon.png',
                ),
                colorFilter:
                    const ColorFilter.mode(Colors.black, BlendMode.color),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: const [
                    Text(
                      'MovieApp',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 36.0,
                        fontWeight: FontWeight.bold,
                      ),
                      // style: Theme.of(context).textTheme.headline1,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                     'Busca tus peliculas favoritas y ve en que plataforma estan disponibles',
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        color: Color(0xff5f5fff),
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: MaterialButton(
                    elevation: 10.0,
                    minWidth: 170.0,
                    height: 50.0,
                    color: Theme.of(context).primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: const Text(
                      'Comencemos',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const HomePage()));
                    }),
              )
            ]),
      ),
    );
  }
}
