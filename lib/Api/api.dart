// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:moviesapp/Model/movies_model.dart';

class FetchMovieList {
  var data = [];
  List<MoviesList> results = [];
  //String urlList = 'http://18.222.196.14:4000/api';
  //String urlList = 'https://moviesappcl.herokuapp.com/api';
    String urlList = 'https://nodeherokucl.herokuapp.com/movies';
  Future<List<MoviesList>> getmovieList({String? query}) async {
    var url = Uri.parse(urlList);

    try {
      var response = await http.get(url);

      if (response.statusCode == 200) {
        // data = json.decode(response.body);
        data = json.decode(response.body);
        
        results = data.map((e) => MoviesList.fromJson(e)).toList();
        if (query != null) {
        results = results
              .where((element) =>  
                  //Este codigo muestra resultado exacto
                  //element.name!.toLowerCase() == (query.toLowerCase()))
                    
                  //Es codigo muestra los que se parece
                 element.name!.toLowerCase().contains((query.toLowerCase())))
                // regExp.hasMatch(element.name!.toLowerCase()))
              .toList();
              //Este metodo las ordena por orden alfabetico
           //results.sort(((a, b) => a.name!.compareTo(b.name!)));

           //Este metodo funciona correctamente eliminando case sensitive
           results.sort(((a, b) => a.name!.length - b.name!.length));
           
           
          

        }
      } else {
        print('Fetch Error');
      }
    } on Exception catch (e) {
      print('Error:  $e');
    }
    //results.sort();
    return results;
  }
}
