import 'package:flutter/material.dart';
import 'package:moviesapp/Api/api.dart';
import 'package:url_launcher/url_launcher.dart';
import 'movies_model.dart';

class SearchMovie extends SearchDelegate {
  final FetchMovieList _movieList = FetchMovieList();

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      Container(
        color: Colors.black,
        child: IconButton(
          onPressed: () {
            query = '';
          },
          icon: const Icon(
            Icons.close,
            color: Colors.white,
          ),
        ),
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return Container(
      color: Colors.black,
      child: IconButton(
        icon: const Icon(
          Icons.arrow_back_ios,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container(
      color: Colors.black,
      child: FutureBuilder<List<MoviesList>>(
          future: _movieList.getmovieList(query: query),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            List<MoviesList>? data = snapshot.data;

            return Container(
              color: Colors.black,
              child: ListView.builder(

                  //scrollDirection: Axis.horizontal,
                  itemCount: data?.length,
                  itemBuilder: (context, index) {
                    final url = '${data?[index].urlMovie}';

                    return Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 10),
                            child: SizedBox(
                              height: 300,
                              width: 350,
                              child: Container(
                                padding: const EdgeInsets.all(0.0),
                                width: 30.0,
                                child: IconButton(
                                  //  constraints: BoxConstraints.tight(const Size(24, 24)),
                                  // iconSize: 10,
                                  onPressed: () => _launchURL(url),
                                  icon: Image.network(
                                    '${data?[index].image}',
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                            ),
                          ),

                          //  const SizedBox(width: 10),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${data?[index].name}',
                                    softWrap: true,
                                    //    textAlign: TextAlign.start,
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 25,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  const SizedBox(height: 10.0),
                                 Image.asset(
                                     //'assets/icons/star-plus-logo.png.',
                                    '${data?[index].platform}',
                                    height: 50.0,
                                    width: 85.0,
                                  )

                                  /* Text(
                                '${data?[index].platform}',
                                //    textAlign: TextAlign.left,
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              */
                                ]),
                          )
                        ]);
                  }),
            );
          }),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container(
      color: Colors.black,
      child: const Center(
        child: Text(
          'Buscar Pelicula...',
          style: TextStyle(color: Colors.white, fontSize: 20.0),
        ),
      ),
    );
  }
}

//ENVIA A URL EN IMAGEN
_launchURL(url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
