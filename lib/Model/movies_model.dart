class MoviesList {
  int? id;
  String? name;
  String? platform;
  String? image;
  String? urlMovie;

  MoviesList({this.id, this.name, this.platform, this.image, this.urlMovie});

  MoviesList.fromJson(Map<String, dynamic> json) {
    id = json['id_movie'];
    name = json['movie_title'];
    platform = json['platform'];
    image = json['img_movie'];
    urlMovie = json['url_movie'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id_movie'] = id;
    data['movie_title'] = name;
    data['platform'] = platform;
    data['img_movie'] = image;
    data['url_movie'] = urlMovie;
    return data;
  }
}
